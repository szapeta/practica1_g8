const { response, request } = require('express');

const funcionesGet = async (req = request, res = response) => {
    res.json({
        msg: 'get API',
    });
};

const funcionesPut = (req = request, res = response) => {
    const { id } = req.params;
    const idUser = parseInt(id);
    res.json({
        msg: 'put API',
        idUser
    });
};

const funcionesPost = async (req = request, res = response) => {
    const { username } = req.body;
    res.json({
        msg: 'post API',
    });
};

const funcionesDelete = (req = request, res = response) => {
    const { id } = req.params;
    const idusuario = parseInt(id);
    res.json({
        msg: 'delete API',
        idusuario
    });
};

module.exports = {
    funcionesGet,
    funcionesPost,
    funcionesPut,
    funcionesDelete,
}