const express = require('express');
const xmlparser = require('express-xml-bodyparser');
const cors = require('cors');

class Server {

    constructor() {
        this.app = express();
        this.server = require('http').createServer(this.app);
        this.io = require('socket.io')(this.server, {
            cors: {
                origin: '*',
            }
        })

        this.funciones = '/funciones'
        
        this.middlewares();
        this.routes();

    }

    middlewares() {
        this.app.use(cors());
        this.app.use(xmlparser());
        this.app.use(express.json());
        this.app.use(express.static('public'));
    }

    routes() {
        this.app.use('/', require('../routes/inicio'));
        this.app.use(this.funciones, require('../routes/funciones'));

        //funcion demo de tipo post
        this.app.post('/postdummy/:count', (req, resp)=>{
            let {count} = req.params;
            resp.json({
                valLeido:count
            })
        })

        this.app.post('/paroimpar', (req, res)=>{
            if(req.body['numero'] % 2 == 0){
                res.send("Par");
            }else{
                res.send("Impar");
            }
        })
        //CAMBVIOOOOO

        //cambio2 fibo

        //DEV1 YA TERMINO

        //DEV1 TERMINO OTRA VEZ
    }

    listen(port) {
        this.server.listen(port, () => {
            console.log('running on ', port);
        });
    }
}

module.exports = Server;